import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
	kotlin("multiplatform")
	id("com.android.library")
}

android {
	namespace = "ic.base"
}

kotlin {

	android()

	val iosTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget = when {
		System.getenv("SDK_NAME")?.startsWith("iphoneos") == true -> ::iosArm64
		else -> ::iosX64
	}

	iosTarget("ios") {
		binaries {
			framework {
				baseName = "base"
			}
		}
	}

	sourceSets {
		val commonMain by getting {
			kotlin.srcDirs("src/common", "src/km")
		}
		val androidMain by getting {
			kotlin.srcDirs("src/jvm", "src/km-jvm", "src/android/java", "src/km-android/java")
			resources.srcDirs("src/android/res")
			dependencies {
				api("androidx.multidex:multidex:2.0.1")
				api("androidx.recyclerview:recyclerview:1.3.0")
				api("androidx.viewpager2:viewpager2:1.0.0")
			}
		}
		val iosMain by getting {
			kotlin.srcDirs("src/native", "src/km-native", "src/ios", "src/km-ios")
		}
	}

	targets.withType<KotlinNativeTarget>().all {
		compilations.all {
			kotlinOptions.freeCompilerArgs += listOf("-memory-model", "experimental")
		}
	}

}

android {
	compileSdk = 33
	sourceSets["main"].manifest.srcFile("src/android/AndroidManifest.xml")
	sourceSets["main"].res.srcDirs("src/android/res")
	defaultConfig {
		minSdk = 21
		targetSdk = 33
	}
}